#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

char checkChar(char c) {
  if (isdigit(c)) {
    return c;
  } else if (c >= 'a' && c <= 'z') {
    return toupper(c);
  }
  return tolower(c);
}

int main(int argc, char* argv[]) {
  char name[30];
  printf("Insert name\n");
  scanf("%s", &name[0]);

  char *start = name;
  char *end = start + strlen(name) -1;
  char temp;

  while(end >= start) {
    temp = checkChar(*start);
    *start = checkChar(*end);
    *end = temp;

    start++;
    end--;
  }

  printf("%s\n", &name[0]);

  return 0;
}
