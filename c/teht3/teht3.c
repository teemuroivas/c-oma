#include <stdio.h>
#include <string.h>

void combineTexts(char a[8], char b[8]) {
  char res[16];
  char *pointer = a;
  int i = 0;
  while(i < strlen(a)) {
    res[i] = *pointer;
    pointer++;
    i++;
  }
  pointer = b;
  i = 0;
  while(i < strlen(b)) {
    res[i + strlen(a)] = *pointer;
    pointer++;
    i++;
  }
  // res[strlen(a) + strlen(b)] = '\0';
  printf("%s\n", &res[0] );
}

int main(int argc, char* argv[]) {
  char input1[8];
  char input2[8];
  char res[16];

  printf("Insert first text\n");
  scanf("%s", &input1[0]);
  getchar();

  printf("Insert second text\n");
  scanf("%s", &input2[0]);

  combineTexts(input1, input2);
  return 0;
}
