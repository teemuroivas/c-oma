#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char nimi[24];
int ika;

void tulostaNimi() {
  system("clear");
  printf("Nimesi on %s\n", &nimi[0]);
  getchar();
  getchar();
}

void kysyIka() {
  system("clear");
  printf("Kuinka vanha olet?\n");
  scanf("%d", &ika);
}

void tulostaIka() {
  system("clear");
  printf("Ikasi on %d\n", ika);
  getchar();
  getchar();
}

void kysyNimi() {
  system("clear");
  printf("Mikä on nimesi?\n");
  scanf("%s", &nimi[0]);
}

void tulostaValikko() {
	char v;

	do
	{
			system("clear");
			printf("---------------------------------Valikko------------------------------");
			printf("\n\n\n\n");
			printf("1. Kysy nimi");
			printf("\n2. Kysy ika");
			printf("\n3. Tulosta nimi");
	 		printf("\n4. Tulosta ika");
			printf("\n5. Lopetus");
			printf("\nValitse:");
			scanf("%c", &v);
			switch (v) {
			case '1':
        kysyNimi();
        break;
			case '2':
        kysyIka();
        break;
			case '3':
        tulostaNimi();
        break;
			case '4':
        tulostaIka();
        break;
			case '5':
						break;
			}
	}
	while (v!='5');
}


int main(int argc, char* argv[]) {
  tulostaValikko();
  return 0;
}
