#pragma once
#include "stdafx.h"

#define HELSINKIHINTA 3.20 // vaihtoehto 2: const float HELSINKIHINTA = (float)3.20;
#define SEUTUHINTA 4.50

using namespace std;

enum Matkatyyppi {HELSINKI, SEUTU}; // lueteltu tietotyyppi

class Matkakortti
{
private:
	string *omistajanNimi;
	float *saldo;
public:
	Matkakortti(void);
	//~Matkakortti(void);
	void alusta(string &nimi); 
	void lataa(float lisays);
	bool matkusta(Matkatyyppi tyyppi);
	//void tulostaTiedot(); // käyttöliittymä ja sovellus(kerros) menee sekaisin
	string &palautaNimi();
	float &palautaSaldo();
};

