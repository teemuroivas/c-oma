#pragma once
#include "stdafx.h"

using namespace std;

class Matkakortti; // forward-määrittely, luokka käännetään hetken kuluttua

class Leimaaja
{
private:
	string linjanNimi;
	string kortinOmistaja;
	struct tm aikaLeima;
public:
	Leimaaja();
	void kysyLinjanNimi();
	bool leimaa(Matkakortti &kortti, Matkatyyppi tyyppi);
	void tulostaTiedot();
	
};

