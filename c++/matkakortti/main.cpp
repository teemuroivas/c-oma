#include <iostream>
#include <string>
#include <time.h>

#include "Matkakortti.h"

using namespace std;

int main(int argc, char* argv[]) {
  Matkakortti kortti;
	int v;
	string rivi;
	float raha;
	do
	{
		system("clear");
		cout <<"-------------------Matkakortin testausvalikko--------------------";
		cout <<"\n\n\n\n";
		cout << "\t\t\t\tAlusta matkakortti 1.\n";
		cout << "\t\t\t\tLataa matkakortti 2.\n";
		cout << "\t\t\t\tMatkusta: Helsinki 3.\n";
		cout << "\t\t\t\tMatkusta: Seutu 4.\n";
		cout << "\t\t\t\tTulosta kortin tiedot 5.\n";
		cout << "\t\t\t\tLopeta 6.\n";
		cin >> v;
		switch (v)
		{
			case 1:
        getchar();
				cout << "Anna kortin omistajan nimi: ";
				getline(cin, rivi);
				kortti.alusta(rivi);
			break;
			case 2:
				cout << "Anna ladattava maara: ";
        cin >> raha;
        kortti.lataa(raha);
        getchar();
        getchar();
			break;
			case 3:
        kortti.matkusta(HELSINKI);
        getchar();
        getchar();
			break;
			case 4:
        kortti.matkusta(SEUTU);
        getchar();
        getchar();
			break;
			case 5:
        kortti.getInfo();
        getchar();
        getchar();
				//cin.get();
			break;
			case 6:       //

				cin.get();
			break;

		}
	}
	while (v!=6);
	return 0;
}
