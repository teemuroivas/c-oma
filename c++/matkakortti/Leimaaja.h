#pragma once

#include <string>
#include <time.h>

using namespace std;

struct Leimaus {
  string linjanNimi;
  string kortinOmistaja;
  //time_t leimausAika;
};

class Leimaaja {
  private:
    //Leimaus *leimaus;
    int *index;
  public:
  	Leimaaja(void);
  	string kysyLinjanNimi();
  	void leimaa();
};
