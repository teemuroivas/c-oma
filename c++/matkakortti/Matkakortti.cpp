#include <string>
#include <iostream>

#include "Matkakortti.h"

using namespace std;

Matkakortti::Matkakortti(void) {
  omistaja = new string;
  saldo = new float;
}

Matkakortti::~Matkakortti(void) {
  delete omistaja;
  delete saldo;
}

void Matkakortti::alusta(string &nimi) {
  *omistaja = nimi;
  *saldo = 0;
}

void Matkakortti::getInfo() {
  system("clear");
  cout << "Kortin omistaja: " << *omistaja << endl;
  cout << "Kortin saldo: " << *saldo << " euroa";
}

void Matkakortti::lataa(float &eur) {
  system("clear");
  *saldo += eur;
  cout << "Kortille ladattu " << eur << " euroa." << endl;
  cout << "Kortilla on nyt " << *saldo << " euroa";
}

bool Matkakortti::matkusta(Matkatyyppi tyyppi) {
  system("clear");
  if (tyyppi == HELSINKI) {
    if (*saldo >= (float)HELSINKIHINTA) {
      *saldo -= (float)HELSINKIHINTA;
      cout << "Helsigin lippu ostettu. Lipun hinta " << (float)HELSINKIHINTA << " euroa" << endl;
      cout << "Kortin saldo: " << *saldo << " euroa";
      return true;
    } else {
      cout << "Kortilla ei ole tarpeeksi arvoa. Kortin arvo: " << *saldo << " euroa";
    }
  } else if (tyyppi == SEUTU) {
    if (*saldo >= (float)SEUTUHINTA) {
      *saldo -= (float)SEUTUHINTA;
      cout << "Seutulippu ostettu. Lipun hinta " << (float)SEUTUHINTA << " euroa" << endl;
      cout << "Kortin saldo: " << *saldo << " euroa";
      return true;
    } else {
      cout << "Kortilla ei ole tarpeeksi arvoa. Kortin arvo: " << *saldo << " euroa";
    }
  }
  return false;
}
