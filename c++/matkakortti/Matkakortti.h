#pragma once

#include <string>

#define HELSINKIHINTA 3.20 // vaihtoehto 2: const float HELSINKIHINTA = (float)3.20;
#define SEUTUHINTA 4.50

using namespace std;

enum Matkatyyppi {HELSINKI, SEUTU}; // lueteltu tietotyyppi

class Matkakortti
{
private:
	string *omistaja;
	float *saldo;
public:
	Matkakortti(void);
  ~Matkakortti(void);
	void alusta(string &nimi);
  void getInfo();
  void lataa(float &eur);
  bool matkusta(Matkatyyppi tyyppi);
};
